package com.app.dao;

import java.util.List;

import com.app.pojos.Veggies;

public interface IveggieDao {
		List<Veggies> findAll();
		Veggies getProduct(int id);
		List<Veggies> getProductByCatagory(String cat);
	  String addProduct(Veggies v);
	  String updateProduct(Veggies v);
	  String deleteProduct(int id);
}
