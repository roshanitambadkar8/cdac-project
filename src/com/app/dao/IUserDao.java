package com.app.dao;

import java.util.List;

import com.app.pojos.Users;

public interface IUserDao {

	List<Users> getUsers();
	String registerUser(Users user);
	String login(String email,String password);
}
