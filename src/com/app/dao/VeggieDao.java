package com.app.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.pojos.Veggies;
@Repository
@Transactional
public class VeggieDao implements IveggieDao{
	@Autowired
	private SessionFactory sf;

	@Override
	public List<Veggies> findAll() {
		String jpql = "select v from Veggies v";
		return sf.getCurrentSession().createQuery(jpql, Veggies.class).getResultList();
	}

	@Override
	public String addProduct(Veggies v) {
		System.out.println(v);
		sf.getCurrentSession().save(v);
		return "product added successfully ";
	}

	@Override
	public String deleteProduct(int id) {
		Session hs = sf.getCurrentSession();
		Veggies v = hs.get(Veggies.class, id);
		if (v != null) {
			hs.delete(v);
			return "Product deleted";
		} else 
			return "Product deletion failed";
	}
	

	@Override
	public String updateProduct(Veggies v) {
		sf.getCurrentSession().update(v);
		return "Product updated Successfully";
	}

	@Override
	public Veggies getProduct(int id) {
		Veggies v=sf.getCurrentSession().get(Veggies.class, id);
		return v;
	}

	@Override
	public List<Veggies> getProductByCatagory(String cat) {
		String jpql="select v from Veggies v where catagory=:cat";
		
		
		return sf.getCurrentSession().createQuery(jpql, Veggies.class).setParameter("cat",cat).getResultList();
	}
	
}
