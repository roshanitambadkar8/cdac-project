package com.app.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Users;

@Repository
public class UserDao implements IUserDao{
	@Autowired
	private SessionFactory sf;
	
	public UserDao() {
		System.out.println("User dao created");
		
	}

	@Override
	public List<Users> getUsers() {
		String jpql="select u from Users u";
		
		return sf.getCurrentSession().createQuery(jpql, Users.class).getResultList();
	}

	@Override
	public String registerUser(Users user) {
		sf.getCurrentSession().persist(user);
		return "User registered successfully";
	}

	@Override
	public String login(String email, String password) {
		String jpql="select u from Users u where email=:em and password=:pw";
		Users u= sf.getCurrentSession().createQuery(jpql, Users.class).setParameter("em", email).setParameter("pw", password).getSingleResult();
		System.out.println(u);
		
		if(u.getRole().equals("Vendor")) {
			System.out.println("vendor");
			return "Vendor";	
		}else if(u.getRole().equals("User")) {
			System.out.println("user");
			return "User";
		}else if(u.getRole().equals("Admin")) {
			System.out.println("admin");
			return "Admin";
		}else
		return "Invalid User...Register First...";
	}
	
	
}
