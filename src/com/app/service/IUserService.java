package com.app.service;

import java.util.List;

import com.app.pojos.Users;
import com.app.pojos.Veggies;

public interface IUserService {
	//Users createUser();
	//Users validateUser(Users user);
	//List<Veggies> getListByCatagory(String catagoryName);
	List<Users> getUser();
	String registerUser(Users user);
	
	String login(String email,String password);
}
