package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.UserDao;
import com.app.pojos.Users;
import com.app.pojos.Veggies;

@Service
@Transactional
public class UserService implements IUserService {
	@Autowired
	private UserDao user;
	
	public UserService() {
		System.out.println("user service created..");
	}

	@Override
	public List<Users> getUser() {
		List<Users> users=user.getUsers();
		return users;
	}
	@Override
	public String registerUser(Users newuser) {
	
		return user.registerUser(newuser);
	}

	@Override
	public String login(String email, String password) {
		
		return user.login(email,password);
	}

	

}
