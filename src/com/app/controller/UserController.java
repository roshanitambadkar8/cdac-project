package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Login;
import com.app.pojos.Users;
import com.app.service.UserService;

@CrossOrigin
@RestController
@EnableAspectJAutoProxy(proxyTargetClass = true)
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService service;
	
	public UserController() {
		System.out.println("User controller created");
	}
	@GetMapping("/list")
	public List<Users> getUsers(){
		List<Users>users= service.getUser();
		for (Users user : users) {
			System.out.println("user details:"+user.toString());
			
		}
		return users;
	}
	
	@PostMapping("/register")
	public ResponseEntity<String> registerUser(@RequestBody Users user) {
		try {
			System.out.println(user);
			return new ResponseEntity<>(service.registerUser(user),HttpStatus.CREATED);
		}catch(RuntimeException e) {
			System.out.println("err in reg"+e);
			return new ResponseEntity<>("user registration failed:"+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	@PostMapping("/login")
	public ResponseEntity<String> login(@RequestBody Login login) {
		String email=login.getEmail();
		String password=login.getPassword();
		try {
			System.out.println(login);
			
			return new ResponseEntity<>(service.login(email,password),HttpStatus.CREATED);
		}catch(RuntimeException e) {
			System.out.println("err in reg"+e);
			return new ResponseEntity<>("user login failed:"+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
