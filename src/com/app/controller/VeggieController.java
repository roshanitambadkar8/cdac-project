package com.app.controller;


import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.app.dao.VeggieDao;
import com.app.pojos.Veggies;

@CrossOrigin//(origins = "http://localhost:4200", allowedHeaders="*")
@RestController
@RequestMapping("/veggies")
public class VeggieController {
	@Autowired
	private VeggieDao dao;
		
	@PostMapping("/upload")
	public ResponseEntity<String> addVegieeWithImage(@RequestParam MultipartFile image, 
			@RequestParam String name,
			@RequestParam String catagory,
			@RequestParam String description, 
			@RequestParam float price, @RequestParam String status) {
		
		
		Veggies v = new Veggies(name, catagory, price, description, status);
		try {
			v.setImage(image.getBytes());
			System.out.println("veggie with image " + name);
			System.out.println(v);
			return new ResponseEntity<>(dao.addProduct(v),HttpStatus.CREATED);
			
		} catch (Exception e) {
			
			e.printStackTrace();	
			return new ResponseEntity<>("Product add failed",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@GetMapping("/listProduct")
	public ResponseEntity<List<Veggies>> getVeggies() {
		return new ResponseEntity<>(dao.findAll(),HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteProduct(@PathVariable int id){
		try {
			System.out.println(id);
			
			return new ResponseEntity<>(dao.deleteProduct(id),HttpStatus.CREATED);
		}catch(RuntimeException e) {
			System.out.println("err in reg"+e);
			return new ResponseEntity<>("Product deletion failed"+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping("/{id}")
	public ResponseEntity<?> getProduct(@PathVariable int id){
		try {
			System.out.println(id);
			
			return new ResponseEntity<>(dao.getProduct(id),HttpStatus.OK);
		}catch(RuntimeException e) {
			System.out.println("err in reg"+e);
			return new ResponseEntity<>("Product not found"+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/update")
	public ResponseEntity<String> updateProduct(@RequestBody Veggies veggie){
		try {
			System.out.println(veggie);
			
			return new ResponseEntity<>(dao.updateProduct(veggie),HttpStatus.CREATED);
		}catch(RuntimeException e) {
			System.out.println("err in reg"+e);
			return new ResponseEntity<>("Product not found"+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/catagory/{catagory}", method = RequestMethod.GET)
public List<Veggies> getVeggiesByCatagory(@PathVariable String catagory) {
		return dao.getProductByCatagory(catagory);
	}
}
