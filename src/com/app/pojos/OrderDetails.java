package com.app.pojos;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="order_details")
public class OrderDetails {
private Integer orderNo;
private Date orderDate;
private Date deliveryDate;
private int qty;
private float totalPrice;
private String status;
private Cart cart;
private DeliveryPerson delivery;

public OrderDetails() {
	System.out.println("inside order details");
}
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
public Integer getOrderNo() {
	return orderNo;
}

public void setOrderNo(Integer orderNo) {
	this.orderNo = orderNo;
}
@Column(name="order_date")
public Date getOrderDate() {
	return orderDate;
}

public void setOrderDate(Date orderDate) {
	this.orderDate = orderDate;
}
@Column(name="delivery_date")
public Date getDeliveryDate() {
	return deliveryDate;
}

public void setDeliveryDate(Date deliveryDate) {
	this.deliveryDate = deliveryDate;
}

public int getQty() {
	return qty;
}

public void setQty(int qty) {
	this.qty = qty;
}

public float getTotalPrice() {
	return totalPrice;
}

public void setTotalPrice(float totalPrice) {
	this.totalPrice = totalPrice;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}

@OneToOne
@JoinColumn(name="cart_id")
public Cart getCart() {
	return cart;
}
public void setCart(Cart cart) {
	this.cart = cart;
}

@OneToOne(mappedBy="order",cascade=CascadeType.ALL)
public DeliveryPerson getDelivery() {
	return delivery;
}
public void setDelivery(DeliveryPerson delivery) {
	this.delivery = delivery;
}
@Override
public String toString() {
	return "OrderDetails [orderNo=" + orderNo + ", orderDate=" + orderDate + ", deliveryDate=" + deliveryDate + ", qty="
			+ qty + ", totalPrice=" + totalPrice + ", status=" + status + ", cart=" + cart + ", delivery=" + delivery
			+ "]";
}


}
	

