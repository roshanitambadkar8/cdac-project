package com.app.pojos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="delivery_person")
public class DeliveryPerson {
	private Integer dId;
	private String firstName;
	private String lastName;
	private int pincode;
	private String city;
	private String phone_no;
	private String status;
	private OrderDetails order;
	
	
	public DeliveryPerson() {
		System.out.println("in delivery person ctor");	
		}

	public DeliveryPerson(Integer dId, String firstName, String lastName, int pincode, String city, String phone_no) {
		super();
		this.dId = dId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.pincode = pincode;
		this.city = city;
		this.phone_no = phone_no;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getdId() {
		return dId;
	}

	public void setdId(Integer dId) {
		this.dId = dId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhone_no() {
		return phone_no;
	}

	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@OneToOne
	@JoinColumn(name="order_id")
	public OrderDetails getOrder() {
		return order;
	}

	public void setOrder(OrderDetails order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "DeliveryPerson [dId=" + dId + ", firstName=" + firstName + ", lastName=" + lastName + ", pincode="
				+ pincode + ", city=" + city + ", phone_no=" + phone_no + ", status=" + status + ", order=" + order
				+ "]";
	}
	
}
