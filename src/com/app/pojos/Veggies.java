package com.app.pojos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="veggies")
public class Veggies {
	private Integer vid;
	private String name;
	private String catagory;
	private float price;
	private String description;
	private String status;
	private Cart cart;
	private byte[] image;
	
	public Veggies() {
		System.out.println("inside veggies ctor..");
	}
	

	public Veggies(String name, String catagory, float price, String description, String status) {
		super();
		this.name = name;
		this.catagory = catagory;
		this.price = price;
		this.description = description;
		this.status = status;
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getVid() {
		return vid;
	}

	public void setVid(Integer vid) {
		this.vid = vid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCatagory() {
		return catagory;
	}

	public void setCatagory(String catagory) {
		this.catagory = catagory;
	}
	
	@Lob
	public byte[] getImage() {
		return image;
	}


	public void setImage(byte[] image) {
		this.image = image;
	}


	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	@ManyToOne
	@JoinColumn(name="veg_id")
	public Cart getCart() {
		return cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}

	@Override
	public String toString() {
		return "Veggies [vid=" + vid + ", name=" + name + ", catagory=" + catagory + ", price=" + price
				+ ", description=" + description + ", status=" + status + ", cart=" + cart + ", image=" + image + "]";
	}
	
	
}
