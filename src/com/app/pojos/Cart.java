package com.app.pojos;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_cart")
public class Cart {
	private Integer cartId;
	private List<Veggies> veggies;
	private int quantity;
	private Users user;
	private OrderDetails orderDetails;

	
	public Cart() {
		System.out.println("in Cart..");
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cart_id")
	public Integer getCartId() {
		return cartId;
	}

	public void setCartId(Integer cartId) {
		this.cartId = cartId;
	}
	
	@OneToOne
	@JoinColumn(name="user_id")
	public Users getUser() {
		return user;
	}
	public void setUser(Users user) {
		this.user = user;
	}
	@OneToMany(mappedBy="cart",cascade=CascadeType.ALL)
	public List<Veggies> getVeggies() {
		return veggies;
	}
	public void setVeggies(List<Veggies> veggies) {
		this.veggies = veggies;
	}
	@OneToOne(mappedBy="cart",cascade=CascadeType.ALL)
	public OrderDetails getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	@Override
	public String toString() {
		return "Cart [cartId=" + cartId + ", veggies=" + veggies + ", quantity=" + quantity + ", user=" + user
				+ ", orderDetails=" + orderDetails + "]";
	}
		
	
}
